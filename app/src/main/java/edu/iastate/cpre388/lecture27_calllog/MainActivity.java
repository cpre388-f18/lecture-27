package edu.iastate.cpre388.lecture27_calllog;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresPermission;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.Date;

public class MainActivity extends AppCompatActivity {
    /** The main ListView to display phone log events */
    private ListView callListView;
    /** The permissions request code for read access */
    private static final int READ_CALL_LOG_REQUEST = 1;
    /** The permissions request code for write access */
    private static final int WRITE_CALL_LOG_REQUEST = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Find the instance of the TextView for later access.
        callListView = findViewById(R.id.callListView);

        // Use a helper function to attempt to populate the ListView with phone log events.
        tryConnectListToProvider();
    }

    /**
     * A non-blocking (async) helper function for attempting to populate the ListView with the
     * phone log. This may request the READ_CALL_LOG permission.
     */
    private void tryConnectListToProvider() {
        // Check if this app has the read permission.
        if (checkSelfPermission(Manifest.permission.READ_CALL_LOG)
                == PackageManager.PERMISSION_GRANTED) {
            // This app has permission to read the call log, so issue a query for the full call log.
            ContentResolver resolver = getContentResolver();
            Cursor resultCursor;
            // The columns from the call log that we need.  _ID is needed for the
            // SimpleCursorAdapter.
            String[] projectionColumns = {
                    CallLog.Calls._ID,
                    CallLog.Calls.NUMBER,
                    CallLog.Calls.DATE,
                    CallLog.Calls.DURATION
            };
            // No filter (select all rows)
            String selection = null;
            // Since the selection has no arguments ('?' in selection), empty array.
            String[] selectionArgs = {};
            // The selection order gets newest calls first.  Evaluates to "date DESC".
            String resultOrder = CallLog.Calls.DATE + " DESC";

            // Execute the query to the content provider.  Gets a cursor result.
            resultCursor = resolver
                    .query(
                            CallLog.Calls.CONTENT_URI,
                            projectionColumns,
                            selection,
                            selectionArgs,
                            resultOrder);


            // USE THE CURSOR WITH AN ADAPTER TO POPULATE THE LISTVIEW.
            // The column name in the cursor that will be mapped to Views.
            String[] rowFrom = {
                    CallLog.Calls.NUMBER,
                    CallLog.Calls.DATE,
                    CallLog.Calls.DURATION
            };
            // The ID of the Views to populate in each row using the data from the cursor.
            int[] rowToId = {
                    R.id.callNumTextView,
                    R.id.callDateTextView,
                    R.id.callDurationTextView
            };

            // Construct the adapter, using the above mapping, and the Cursor from the content
            // provider.
            SimpleCursorAdapter adapter = new SimpleCursorAdapter(
                    this,
                    R.layout.phone_row_layout,
                    resultCursor,
                    rowFrom,
                    rowToId,
                    0);
            // Connect the ListView to the Adapter that is connected to the content provider.
            callListView.setAdapter(adapter);
        } else {
            // Call log read permission is not held.
            Toast.makeText(this, R.string.no_log_read_perm, Toast.LENGTH_LONG).show();

            String[] requiredPerm = {Manifest.permission.READ_CALL_LOG};
            // Start an async call that will raise a callback.  This function returns immediately,
            // before the user has made a decision to approve or deny.
            requestPermissions(requiredPerm, READ_CALL_LOG_REQUEST);
        }
    }

    // The callback after calling requestPermissions().
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        // Since requestPermissions() is called in multiple places, distinguishing requestCode is
        // important.
        switch (requestCode) {
            case READ_CALL_LOG_REQUEST: // Read permission was approved or denied.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission was granted, read the call log.
                    tryConnectListToProvider();
                } else {
                    // Permission was denied by the user.
                    Toast.makeText(this, R.string.no_log_read_perm, Toast.LENGTH_LONG).show();
                }
                break;
            case WRITE_CALL_LOG_REQUEST: // Write permission was approved or denied.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission was granted, try insert again
                    tryInsertCall();
                } else {
                    // Permission was denied by the user.
                    Toast.makeText(this, R.string.no_log_write_perm, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    /**
     * Event handler for a new call log button click.
     * @param v the new event log button
     */
    public void onNewClick(View v) {
        // Simply call the helper function that will either insert the call event to the log (if
        // the permission is already held), or request permission and add it if the user approves
        // the permission.
        tryInsertCall();
    }

    /**
     * Helper function that inserts a call log event, or requests the write permission and adds the
     * event to the log if the user approves the permission. Note: if the user denies, no event will
     * be added.
     */
    private void tryInsertCall() {
        if (checkSelfPermission(Manifest.permission.WRITE_CALL_LOG)
                == PackageManager.PERMISSION_GRANTED) {
            // The write permission is held by this app.
            Uri newCall;
            // Create a new row of data to insert into the content provider.
            ContentValues values = new ContentValues();

            // Set each column value.  The data type and meaning is found in the CallLog.Calls
            // documentation.
            values.put(CallLog.Calls.DATE, new Date().getTime());
            values.put(CallLog.Calls.DURATION, 133);
            values.put(CallLog.Calls.NUMBER, "133-755-0000");
            values.put(CallLog.Calls.TYPE, CallLog.Calls.INCOMING_TYPE);

            // Tell the content provider to insert the new row.
            // NOTE! this runs on the main thread.  This is BAD!  You should use a different thread.
            newCall = getContentResolver().insert(
                    CallLog.Calls.CONTENT_URI,
                    values
            );
            Toast.makeText(this, "New call event URI: " + newCall.toString(),
                    Toast.LENGTH_LONG).show();
        } else {
            // The permission is not held.  Request it, and let the callback try again, if the user
            // approved the permission.
            String[] requiredPerm = {Manifest.permission.WRITE_CALL_LOG};
            requestPermissions(requiredPerm, WRITE_CALL_LOG_REQUEST);
        }
    }
}
